import { test, expect } from '@playwright/test';
import { readFileSync } from "fs"
import { SignUpPage } from '../src/pageobjects/SignUpPage';
import { DataProvider } from '../src/DataProvider';
import { Localization } from '../src/localization'
import { Actions } from '../src/Actions';
import { CheckYourEmailPage } from '../src/pageobjects/CheckYourEmailPage';


const localizationJson = readFileSync("./src/localization.json")
const localization: Localization = JSON.parse(localizationJson.toString())

test.describe('Sign Up Localization |', async () => {

    test.beforeEach(async ({ page }) => {
        await page.goto("https://miro.com/")
    })

    localization.locales.forEach(l => {

        test(`${l.locale} | Signup page text should be of the right locale`, async ({ page }) => {
            //ARRANGE
            await Actions.InitiateSignupFromHomePage(page, l.locale)

            //ASSERT
            const signUpPage = new SignUpPage(page)
            expect(await signUpPage.getStartedLabel.textContent()).toBe(l.signUpPage.getStartedLabel)
            expect(await signUpPage.noCardRequiredLabel.textContent()).toBe(l.signUpPage.noCardRequiredLabel)
            expect(await signUpPage.termsAndPrivacyPolicyLabel.textContent()).toBe(l.signUpPage.termsAndPrivacyLabel)
            expect(await signUpPage.newsAndUpdatesLabel.textContent()).toBe(l.signUpPage.newsAndUpdatesLabel)
            expect(await signUpPage.getStartedNowButton.textContent()).toBe(l.signUpPage.getStartedNowText)
            expect(await signUpPage.getNameFieldPlaceholderText()).toBe(l.signUpPage.namePlaceholder)
            expect(await signUpPage.getEmailFieldPlaceholderText()).toBe(l.signUpPage.emailPlaceholder)
            expect(await signUpPage.getPasswordFieldPlaceholderText()).toBe(l.signUpPage.passwordPlaceholder)
        })

        test(`${l.locale} | Check your email page text should be of the right locale`, async ({ page }) => {
            //ARRANGE
            await Actions.InitiateSignupFromHomePage(page, l.locale)
            const details = DataProvider.healthySignupDetails();

            //ACT
            const signUpPage = new SignUpPage(page)
            await Actions.FillAndSubmitSignupForm(signUpPage, details)

            //ASSERT
            const confirmationPage = new CheckYourEmailPage(page)
            expect(await confirmationPage.checkYourEmailLabel.textContent()).toBe(l.checkYourEmailPage.checkYourEmailLabel)
            expect(await confirmationPage.emailCodeInformationLabel.textContent()).toBe(l.checkYourEmailPage.emailCodeInformationLabel.replace("PLACEHOLDER", details.email))
            expect(await confirmationPage.getSixDigitPlaceholderText()).toBe(l.checkYourEmailPage.sixDigitCodePlaceholder)
            expect((await confirmationPage.sendCodeOrGetHelpLabel.textContent()).trim()).toBe(l.checkYourEmailPage.sendCodeOrGetHelpLabel.trim())
        
        })
    })
})


