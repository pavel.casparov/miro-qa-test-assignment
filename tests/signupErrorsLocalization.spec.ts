import { test, expect } from '@playwright/test';
import { readFileSync } from "fs"
import { SignUpPage } from '../src/pageobjects/SignUpPage';
import { DataProvider } from '../src/DataProvider';
import { Localization } from '../src/localization'
import { Actions } from '../src/Actions';


const localizationJson = readFileSync("./src/localization.json")
const localization: Localization = JSON.parse(localizationJson.toString())

test.describe('Sign Up Errors Localization |', async () => {

  test.beforeEach(async ({ page }) => {
    await page.goto("https://miro.com/")
  })

  localization.locales.forEach(l => {

    test(`${l.locale} | Should throw fields validation errors when signing up with empty details`, async ({ page }) => {
      //ARRANGE
      await Actions.InitiateSignupFromHomePage(page, l.locale)
      const details = DataProvider.emptySignupDetails();

      //ACT
      const signUpPage = new SignUpPage(page)
      await Actions.FillAndSubmitSignupForm(signUpPage, details, false)

      //ASSERT
      expect(await signUpPage.nameFieldError.textContent()).toBe(l.signUpPage.pleaseEnterNameErrLabel)
      expect(await signUpPage.emailFieldError.textContent()).toBe(l.signUpPage.pleaseEnterEmailErrLabel)
      expect((await signUpPage.passwordFieldError.textContent()).trim()).toBe(l.signUpPage.pleaseEnterPasswordErrLabel.trim())
      expect(await signUpPage.termsAndPrivacyError.textContent()).toBe(l.signUpPage.pleaseAgreeTermsErrLabel)
    })

    test(`${l.locale} | Should throw email already in use when attempting to sign up with already registered email`, async ({ page }) => {
      //ARRANGE
      await Actions.InitiateSignupFromHomePage(page, l.locale)
      const details = DataProvider.healthySignupDetails();

      //ACT
      const signUpPage = new SignUpPage(page)
      await Actions.FillAndSubmitSignupForm(signUpPage, details)
      await page.goto("https://miro.com/signup/")
      await Actions.FillAndSubmitSignupForm(signUpPage, details)

      //ASSERT
      expect(page.url().endsWith("signup/"));
      expect(await signUpPage.emailFieldError.textContent()).toBe(l.signUpPage.usedEmailErrLabel)
    })

    test(`${l.locale} | Should throw password to short error when password is less than 8 char`, async ({ page }) => {
      //ARRANGE
      await Actions.InitiateSignupFromHomePage(page, l.locale)
      const details = DataProvider.shortPasswordSignupDetails()

      //ACT
      const signUpPage = new SignUpPage(page)
      await signUpPage.fillSignUpDetails(details)

      //ASSERT
      await signUpPage.passwordFieldHint.hover()
      expect(await signUpPage.passwordFieldHint.textContent()).toBe(l.signUpPage.use8plusCharPasswordErrLabel);
    })

    test(`${l.locale} | Should throw Weak password hint if password is not strong enough`, async ({ page }) => {
      //ARRANGE
      await Actions.InitiateSignupFromHomePage(page, l.locale)
      const details = DataProvider.weakPasswordSignupDetails()

      //ACT
      const signUpPage = new SignUpPage(page)
      await signUpPage.fillSignUpDetails(details)

      //ASSERT
      await signUpPage.passwordFieldHint.hover()
      expect(await signUpPage.passwordFieldHint.textContent()).toBe(l.signUpPage.weakPasswordInfolabel)

    })

    test(`${l.locale} | Should throw So-so password hint if password is not strong enough`, async ({ page }) => {
      //ARRANGE
      await Actions.InitiateSignupFromHomePage(page, l.locale)
      const details = DataProvider.sosoPasswordSignupDetails()

      //ACT
      const signUpPage = new SignUpPage(page)
      await signUpPage.fillSignUpDetails(details)

      //ASSERT
      await signUpPage.passwordFieldHint.hover()
      expect(await signUpPage.passwordFieldHint.textContent()).toBe(l.signUpPage.sosoPasswordInfoLabel)

    })

    test(`${l.locale} | Should throw Good password hint if password is not as strong as  could be`, async ({ page }) => {
      //ARRANGE
      await Actions.InitiateSignupFromHomePage(page, l.locale)
      const details = DataProvider.goodPasswordSignupDetails()

      //ACT
      const signUpPage = new SignUpPage(page)
      await signUpPage.fillSignUpDetails(details)

      //ASSERT
      await signUpPage.passwordFieldHint.hover()
      expect(await signUpPage.passwordFieldHint.textContent()).toBe(l.signUpPage.goodPasswordInfoLabel)
    })

    test(`${l.locale} | Should throw Great password hint if password is strong`, async ({ page }) => {
      //ARRANGE
      await Actions.InitiateSignupFromHomePage(page, l.locale)
      const details = DataProvider.greatPasswordSignupDetails()

      //ACT
      const signUpPage = new SignUpPage(page)
      await signUpPage.fillSignUpDetails(details)

      //ASSERT
      await signUpPage.passwordFieldHint.hover()
      expect(await signUpPage.passwordFieldHint.textContent()).toBe(l.signUpPage.greatPasswordInfoLabel)
    })
  })
})
