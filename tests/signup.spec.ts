import { test, expect } from '@playwright/test';
import { SignUpPage } from '../src/pageobjects/SignUpPage';
import { CheckYourEmailPage } from '../src/pageobjects/CheckYourEmailPage';
import { DataProvider } from '../src/DataProvider';
import { Actions } from '../src/Actions';

test.describe('Sign Up |', async () => {

  test.beforeEach(async ({ page }) => {
    await page.goto("https://miro.com/")
    await Actions.InitiateSignupFromHomePage(page, "en")
  })

  test(`Should sign up with all fields on sign up form pupulated`, async ({ page }) => {
    //ARRANGE
    const details = DataProvider.healthySignupDetails();

    //ACT
    const signUpPage = new SignUpPage(page)
    await Actions.FillAndSubmitSignupForm(signUpPage, details)

    //ASSERT
    const confirmationPage = new CheckYourEmailPage(page)
    expect(await confirmationPage.areElementsVisible()).toBe(true)
    expect(await (confirmationPage.emailUsedLabel.textContent())).toBe(details.email)
  })

  test(`Should sign up without being agree to recieve newsletter and update`, async ({ page }) => {
    //ARRANGE
    const details = DataProvider.healthySignupDetails();

    //ACT
    const signUpPage = new SignUpPage(page)
    await Actions.FillAndSubmitSignupForm(signUpPage, details, true, false)

    //ASSERT
    const confirmationPage = new CheckYourEmailPage(page)
    expect(await confirmationPage.areElementsVisible()).toBe(true)
    expect(await (confirmationPage.emailUsedLabel.textContent())).toBe(details.email)
  })

  test(`Should sign up with Weak password`, async ({ page }) => {
    //ARRANGE
    const details = DataProvider.weakPasswordSignupDetails()

    //ACT
    const signUpPage = new SignUpPage(page)
    await Actions.FillAndSubmitSignupForm(signUpPage, details)

    //ASSERT
    const confirmationPage = new CheckYourEmailPage(page)
    expect (await confirmationPage.areElementsVisible()).toBe(true)
  })

  test(`Should sign up with So-so password`, async ({ page }) => {
    //ARRANGE
    const details = DataProvider.sosoPasswordSignupDetails()

    //ACT
    const signUpPage = new SignUpPage(page)
    await Actions.FillAndSubmitSignupForm(signUpPage, details)

    //ASSERT
    const confirmationPage = new CheckYourEmailPage(page)
    expect (await confirmationPage.areElementsVisible()).toBe(true)
  })

  test(`Should sign up with Good password`, async ({ page }) => {
    //ARRANGE
    const details = DataProvider.goodPasswordSignupDetails()

    //ACT
    const signUpPage = new SignUpPage(page)
    await Actions.FillAndSubmitSignupForm(signUpPage, details)

    //ASSERT
    const confirmationPage = new CheckYourEmailPage(page)
    expect (await confirmationPage.areElementsVisible()).toBe(true)
  })

  test(`Should sign up with Great password`, async ({ page }) => {
    //ARRANGE
    const details = DataProvider.greatPasswordSignupDetails()

    //ACT
    const signUpPage = new SignUpPage(page)
    await Actions.FillAndSubmitSignupForm(signUpPage, details)

    //ASSERT
    const confirmationPage = new CheckYourEmailPage(page)
    expect (await confirmationPage.areElementsVisible()).toBe(true)
  })

  test(`Should not be able to sign up without name`, async ({ page }) => {
    //ARRANGE
    const details = DataProvider.noNameSignupDetails();

    //ACT
    const signUpPage = new SignUpPage(page)
    await Actions.FillAndSubmitSignupForm(signUpPage, details)

    //ASSERT
    expect(page.url().endsWith("signup/"));
    await expect(signUpPage.nameFieldError).toBeVisible()
  })

  test(`Should not be able to sign up without email`, async ({ page }) => {
    //ARRANGE
    const details = DataProvider.noEmailSignupDetails();

    //ACT
    const signUpPage = new SignUpPage(page)
    await Actions.FillAndSubmitSignupForm(signUpPage, details)

    //ASSERT
    expect(page.url().endsWith("signup/"));
    await expect(signUpPage.emailFieldError).toBeVisible()
  })

  test(`Should not be able to sign up with already used email`, async ({ page }) => {
    //ARRANGE
    const details = DataProvider.healthySignupDetails();

    //ACT
    const signUpPage = new SignUpPage(page)
    await Actions.FillAndSubmitSignupForm(signUpPage, details)
    await page.goto("https://miro.com/signup/")
    await Actions.FillAndSubmitSignupForm(signUpPage, details)
    //ASSERT
    expect(page.url().endsWith("signup/"));
    await expect(signUpPage.emailFieldError).toBeVisible()
  })

  test(`Should not be able to sign up without password`, async ({ page }) => {
    //ARRANGE
    const details = DataProvider.noPasswordSignupDetails();

    //ACT
    const signUpPage = new SignUpPage(page)
    await Actions.FillAndSubmitSignupForm(signUpPage, details)

    //ASSERT
    expect(page.url().endsWith("signup/"));
    await expect(signUpPage.passwordFieldError).toBeVisible()
  })

  test(`Should not be able to sign up if not agree with terms and privacy`, async ({ page }) => {
    //ARRANGE
    const details = DataProvider.healthySignupDetails();

    //ACT
    const signUpPage = new SignUpPage(page)
    await Actions.FillAndSubmitSignupForm(signUpPage, details, false)

    //ASSERT
    expect(page.url().endsWith("signup/"));
    await expect(signUpPage.termsAndPrivacyError).toBeVisible()
  })
})

