import { test, expect, Page, Locator } from '@playwright/test';
import { SignUpPage } from '../src/pageobjects/SignUpPage';
import { Actions } from '../src/Actions';
import { Localization } from '../src/localization';
import { readFileSync } from 'fs';

const localizationJson = readFileSync("./src/localization.json")
const localization: Localization = JSON.parse(localizationJson.toString())

test.describe('Terms And Privacy |', async () => {

    test.beforeEach(async ({ page }) => {
        await page.goto("https://miro.com/")
    })


    localization.locales.forEach(l => {

        test(`${l.locale} | Terms of service page should be reachable from the sign up page checkbox text link`, async ({ page, context }) => {
            //ARRANGE
            await Actions.InitiateSignupFromHomePage(page, l.locale)
            
            //ACT
            const signUpPage = new SignUpPage(page)
            await OpenNewTabByLinkAndSwitch(page, signUpPage.termsLink)

            //ASSERT
            const pageTitle = await context.pages().find(p => p.url() === 'https://miro.com/legal/terms-of-service/').title();
            expect(pageTitle).toBe('Terms of Service, Online Whiteboard | Miro')
        })

        test(`${l.locale} | Privacy Policy page should be reachable from the sign up page checkbox text link`, async ({ page, context }) => {
            //ARRANGE
            await Actions.InitiateSignupFromHomePage(page, l.locale)
            //ACT
            const signUpPage = new SignUpPage(page)
            await OpenNewTabByLinkAndSwitch(page, signUpPage.privacyPolicyLink)


            //ASSERT
            const pageTitle = await context.pages().find(p => p.url() === 'https://miro.com/legal/privacy-policy/').title();
            expect(pageTitle).toBe('Terms of Service, Online Whiteboard | Miro')
        })
    })

    async function OpenNewTabByLinkAndSwitch(page: Page, linkLocator: Locator) {
        const [newTab] = await Promise.all([
            page.waitForEvent('popup'),
            linkLocator.click(),  // Opens popup
        ]);
        await newTab.waitForLoadState('load')
    }
})


