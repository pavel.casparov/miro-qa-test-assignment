# Miro QA Automation Test Assignement

##### Creating E2E tests that can cover sign up functions in Miro.
###### Assignment requirements and tips:
- You could feel free to choose a coding language among  Javascript/Typescript/Java
- You could choose any test framework such as selenium, playwright, cypress or testcafe etc.
- Please avoid using some BDD Framework such as Cucumber.
- Please add a Readme within the project
- We can consider a successful registration when the "Check your email" screen is visible.
- The way to manage test structure is also as important as designing the test cases from all possible perspectives.


## Test Cases
I managed to come up with 4 groups of test cases
- _Sign Up_ --- Testing the sign up functionality of Miro in the end-to-end manner
- _Sign Up Errors Localization_ --- Testing the localization of the errors and hints with 2 different locales, English and French
- _Sign Up Localization_ --- Testing the localization of the the Sign Up page and Check your email page with 2 different locales, English and French
- _Terms And Privacy_ --- Testing if Terms and Privacy Policy pages are reachable from sign up page no matter of what locale is used English or French

```
Sign Up | Should sign up with all fields on sign up form pupulated
Sign Up | Should sign up without being agree to recieve newsletter and update
Sign Up | Should sign up with Weak password
Sign Up | Should sign up with So-so password
Sign Up | Should sign up with Good password
Sign Up | Should sign up with Great password
Sign Up | Should not be able to sign up without name
Sign Up | Should not be able to sign up without email
Sign Up | Should not be able to sign up with already used email
Sign Up | Should not be able to sign up without password
Sign Up | Should not be able to sign up if not agree with terms and privacy
```

```
Sign Up Errors Localization | en | Should throw fields validation errors when signing up with empty details
Sign Up Errors Localization | en | Should throw email already in use when attempting to sign up with already registered email
Sign Up Errors Localization | en | Should throw password to short error when password is less than 8 char
Sign Up Errors Localization | en | Should throw Weak password hint if password is not strong enough
Sign Up Errors Localization | en | Should throw So-so password hint if password is not strong enough
Sign Up Errors Localization | en | Should throw Good password hint if password is not as strong as  could be
Sign Up Errors Localization | en | Should throw Great password hint if password is strong
Sign Up Errors Localization | fr | Should throw fields validation errors when signing up with empty details
Sign Up Errors Localization | fr | Should throw email already in use when attempting to sign up with already registered email
Sign Up Errors Localization | fr | Should throw password to short error when password is less than 8 char
Sign Up Errors Localization | fr | Should throw Weak password hint if password is not strong enough
Sign Up Errors Localization | fr | Should throw So-so password hint if password is not strong enough
Sign Up Errors Localization | fr | Should throw Good password hint if password is not as strong as  could be
Sign Up Errors Localization | fr | Should throw Great password hint if password is strong
```

```
Sign Up Localization | en | Signup page text should be of the right locale
Sign Up Localization | en | Check your email page text should be of the right locale
Sign Up Localization | fr | Signup page text should be of the right locale
Sign Up Localization | fr | Check your email page text should be of the right locale
```

```
Terms And Privacy | en | Terms of service page should be reachable from the sign up page checkbox text link
Terms And Privacy | en | Privacy Policy page should be reachable from the sign up page checkbox text link
Terms And Privacy | fr | Terms of service page should be reachable from the sign up page checkbox text link
Terms And Privacy | fr | Privacy Policy page should be reachable from the sign up page checkbox text link
```

## Test Automation
To implement test automation of the test cases above a following tech stack was used:
- [NodeJs](https://nodejs.org/en/) --- runtime
- [Typescript](https://www.typescriptlang.org/) --- prgoramming language
- [Playwright](https://playwright.dev/) --- web browser automation library
- [Faker](https://www.npmjs.com/package/faker) --- random data generator library

### How to

Before proceeding with the runing tests you should make sure you have [Git](https://git-scm.com/downloads) and [NodeJs](https://nodejs.org/en/) installed on your machine
Once ready run the following code snippet line by line using your favorite terminal
```sh
git clone https://gitlab.com/pavel.casparov/miro-qa-test-assignment.git
cd miro-qa-test-assignment
npm i
npx playwright install
```
#### Out of the box test cases are configured to run headless meaning no browser will open it's interface. To change this behavior please modify `playwright.config.ts` in accordance to your needs.

>To proceed with tests using Chromium run
```
npm run test:chromium
```

>To proceed with tests using Webkit run
```
npm run test:safari
```

>To proceed with tests using Firefox run
```
npm run test:firefox
```

>If you desire to run a specific test group you can achive it by running the following command
>Thid will run the tests against all 3 browsers
```sh
npx playwright test tests/signup.spec.ts
```
```sh
npx playwright test tests/signupErrorsLocalization.spec.ts
```
```sh
npx playwright test tests/signupLocalization.spec.ts
```
```sh
npx playwright test tests/termsAndPrivacyPolicy.spec.ts
```
###### For additional ways of running tests please consult the [Playwrigth test-cli](https://playwright.dev/docs/test-cli/) documentation

After the test run the test results are produced in accordance to `playwright.config.ts` setup. You can find test results under `test-results` folder. Each test case will have it's own subfolder, and a common `result.json` file will contain test results for the whole test run

### Test runner configuration
Playwright uses it's own config to define a behavior of the test run. You can find it within the `playwright.config.ts` file in the root directory of the project. For more details on how it works please visit [Playwright Configuration](https://playwright.dev/docs/test-configuration) page



### Known issues and things to improve
- Running all test in a single run will lead to inconsisten results due to hardware and network limitation, to avoid it run test by groups (spec files)
- 1 issue was found on Check your email page when the language is no English. The 6 digit code field placeholder still stays in English even though the rest of the page is in another locale
- Italian and Portugues translations are missing for the sign up process, the sign up and check your email pages are still in English. There is no test automation around it but it can be easeally added defining a new locale in the `localization.ts` under the /src folder
- Playwright supports mobile browsers emulation, however I didn't went in that direction. For demo purpose 3 desktop browsers should be enough, although to add mobile support there is not much of a hustle, it will consist in some modification in the page objects and actions to make those work with the mobile representation of the website.
