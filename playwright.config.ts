import { PlaywrightTestConfig } from '@playwright/test';
const config: PlaywrightTestConfig = {
    workers: 4,
    retries: 2,
    reporter: [
        ['list'],
        ['json', { outputFile: 'test-results/results.json' }]
    ],

    use: {
        headless: true,
        ignoreHTTPSErrors: true,
        screenshot: 'only-on-failure',
        trace: 'retain-on-failure',
        actionTimeout: 45*1000,
        navigationTimeout: 45*1000,        
    },

    projects: [
        {
            name: 'Chromium',
            use: {
                browserName: 'chromium',
                viewport: { width: 1280, height: 720 },
            },
        },
        {
            name: 'Safari',
            use: {
                browserName: 'webkit',
                viewport: { width: 1280, height: 720 },
            }
        },
        {
            name: 'Firefox',
            use: {
                browserName: 'firefox',
                viewport: { width: 1280, height: 720 },
            }
        }
    ],
};
export default config;