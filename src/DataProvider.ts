import {name, internet} from "faker"
import { SignUpDetails } from "./dto/SignUpDetails";

export class DataProvider {

static healthySignupDetails = () : SignUpDetails => 
    new SignUpDetails(name.firstName(), internet.email(), internet.password(12))

static noNameSignupDetails = () : SignUpDetails => 
    new SignUpDetails("", internet.email(), internet.password(12))

static noEmailSignupDetails = () : SignUpDetails => 
    new SignUpDetails(name.firstName(), "", internet.password(12))

static noPasswordSignupDetails = () : SignUpDetails => 
    new SignUpDetails(name.firstName(), internet.email(), "")

static emptySignupDetails = () : SignUpDetails =>
    new SignUpDetails()

static invalidEmailSignupDetails = () : SignUpDetails => 
    new SignUpDetails(name.firstName(), "wrongmailformat", internet.password(12))

static shortPasswordSignupDetails = () : SignUpDetails =>
    new SignUpDetails(name.firstName(), internet.email(), "1234567")

static weakPasswordSignupDetails = () : SignUpDetails =>
    new SignUpDetails(name.firstName(), internet.email(), "123456789")

static sosoPasswordSignupDetails = () : SignUpDetails =>
    new SignUpDetails(name.firstName(), internet.email(), "12345678!@#A")

static goodPasswordSignupDetails = () : SignUpDetails =>
    new SignUpDetails(name.firstName(), internet.email(), "12345678!@#A!@")

static greatPasswordSignupDetails = () : SignUpDetails =>
    new SignUpDetails(name.firstName(), internet.email(), "12345678!@#A!@asd!@#")
}