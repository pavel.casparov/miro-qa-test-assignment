export interface Localization {
    locales: Locale[];
}

export interface Locale {
    locale: string;
    urlPostfix: string;
    homePage: HomePage;
    signUpPage: SignUpPage;
    checkYourEmailPage: CheckYourEmailPage;
}

export interface CheckYourEmailPage {
    checkYourEmailLabel: string;
    emailCodeInformationLabel: string;
    sixDigitCodePlaceholder: string;
    sendCodeOrGetHelpLabel: string;
}

export interface HomePage {
    signUpButtonLabel: string;
}

export interface SignUpPage {
    getStartedLabel: string;
    noCardRequiredLabel: string;
    namePlaceholder: string;
    emailPlaceholder: string;
    passwordPlaceholder: string;
    termsAndPrivacyLabel: string;
    newsAndUpdatesLabel: string;
    getStartedNowText: string;
    pleaseEnterNameErrLabel: string;
    pleaseEnterEmailErrLabel: string;
    pleaseEnterPasswordErrLabel: string;
    pleaseAgreeTermsErrLabel: string;
    incorrectEmailErrLabel: string;
    usedEmailErrLabel: string;
    use8plusCharPasswordErrLabel: string;
    weakPasswordInfolabel: string;
    sosoPasswordInfoLabel: string;
    goodPasswordInfoLabel: string;
    greatPasswordInfoLabel: string;
}
