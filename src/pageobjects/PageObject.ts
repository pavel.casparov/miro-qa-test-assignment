import {Locator, Page } from '@playwright/test';

export class PageObject {
    readonly page: Page;

    constructor(page: Page){
        this.page = page;
    }

    async getPlaceholderText (locator: Locator) : Promise<string> {
        return await locator.getAttribute("placeholder") ?? "";
    }
}