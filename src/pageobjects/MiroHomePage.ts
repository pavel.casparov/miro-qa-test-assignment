import { expect, Locator, Page } from '@playwright/test';
import { PageObject } from './PageObject';

export class MiroHomePage extends PageObject {
    readonly signUpFreeButton: Locator;
    readonly languageSwitcher: Locator;

    constructor (page: Page){
        super(page);
        this.signUpFreeButton = page.locator("a[data-autotest-id=header-signup-1]")
        this.languageSwitcher = page.locator("div.header-1__language-switcher-container")
    }

    async clickOnSignupForFree() : Promise<void> {
        await this.signUpFreeButton.click()
    }

    async changeSiteLocale(locale: string) : Promise<void> {
        await this.languageSwitcher.hover();
        await this.page.locator(`li > a[data-local=${locale}]`).hover();
        await this.page.locator(`li > a[data-local=${locale}]`).click();
        await this.page.waitForLoadState('networkidle')
        expect(this.page.url().endsWith(locale));
    }
}
