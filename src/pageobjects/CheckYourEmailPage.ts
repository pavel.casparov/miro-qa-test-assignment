import { Locator, Page } from '@playwright/test';
import { PageObject } from './PageObject';

export class CheckYourEmailPage extends PageObject{
    readonly checkYourEmailLabel: Locator;
    readonly emailCodeInformationLabel: Locator;
    readonly sixDigitCodeField: Locator;
    readonly sendCodeOrGetHelpLabel: Locator;
    readonly emailUsedLabel: Locator;
    readonly reSendCodeLink: Locator;
    readonly helpCenterLink: Locator;

    constructor(page: Page){
        super(page)
        this.checkYourEmailLabel = page.locator("div > h1.signup__title-form")
        this.emailCodeInformationLabel = page.locator("div.signup__subtitle-form")
        this.sixDigitCodeField = page.locator("id=code")
        this.sendCodeOrGetHelpLabel = page.locator("div.signup__footer")
        this.emailUsedLabel = page.locator("div.signup__subtitle-form > strong")
        this.reSendCodeLink = page.locator("div.signup__footer > a:nth-child(1)")
        this.helpCenterLink = page.locator("div.signup__footer > a:nth-child(2)")
    }


    async getSixDigitPlaceholderText() : Promise<string> {
        return await super.getPlaceholderText(this.sixDigitCodeField)
    }

    async areElementsVisible() : Promise<boolean> {
        return await this.checkYourEmailLabel.isVisible() &&
        this.emailCodeInformationLabel.isVisible() &&
        this.sixDigitCodeField.isVisible() &&
        this.sendCodeOrGetHelpLabel.isVisible() &&
        this.emailUsedLabel.isVisible() &&
        this.reSendCodeLink.isVisible() &&
        this.helpCenterLink.isVisible()
    }
}