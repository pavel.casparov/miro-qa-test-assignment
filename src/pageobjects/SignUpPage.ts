import { Locator, Page } from '@playwright/test';
import { SignUpDetails } from '../dto/SignUpDetails';
import { PageObject } from './PageObject';

export class SignUpPage extends PageObject {
    readonly getStartedLabel: Locator;
    readonly noCardRequiredLabel: Locator;
    readonly nameField: Locator;
    readonly emailField: Locator;
    readonly passwordField: Locator;
    readonly termsAndPrivacyPolicyCheckbox: Locator;
    readonly termsAndPrivacyPolicyLabel: Locator;
    readonly newsAndUpdatesCheckbox: Locator;
    readonly newsAndUpdatesLabel: Locator;
    readonly getStartedNowButton: Locator;
    readonly termsLink: Locator;
    readonly privacyPolicyLink: Locator;
    readonly nameFieldError: Locator;
    readonly emailFieldError: Locator;
    readonly passwordFieldHint: Locator;
    readonly passwordFieldError: Locator;
    readonly termsAndPrivacyError: Locator;

    constructor(page: Page) {
        super(page);
        this.getStartedLabel = page.locator("div.signup > h1")
        this.noCardRequiredLabel = page.locator("div.signup > div:nth-child(2)")
        this.nameField = page.locator("input[data-autotest-id=mr-form-signup-name-1]")
        this.emailField = page.locator("input[data-autotest-id=mr-form-signup-email-1]")
        this.passwordField = page.locator("input[data-autotest-id=mr-form-signup-password-1]")
        this.termsAndPrivacyPolicyCheckbox = page.locator("div[data-autotest-id=mr-form-signup-terms-1] > span > label")
        this.termsAndPrivacyPolicyLabel = page.locator("div[data-autotest-id=mr-form-signup-terms-1] > span > span")
        this.newsAndUpdatesCheckbox = page.locator("div[data-autotest-id=mr-form-signup-subscribe-1] > span > label")
        this.newsAndUpdatesLabel = page.locator("div[data-autotest-id=mr-form-signup-subscribe-1] > span > span")
        this.getStartedNowButton = page.locator("button[data-autotest-id=mr-form-signup-btn-start-1]")
        this.termsLink = page.locator("a[data-autotest-id=mr-link-terms-1]")
        this.privacyPolicyLink = page.locator("a[data-autotest-id=mr-link-privacy-1]")
        this.nameFieldError = page.locator("id=nameError")
        this.emailFieldError = page.locator("id=emailError")
        this.passwordFieldHint = page.locator("#password-hint > #signup-form-password:nth-child(2)")
        this.passwordFieldError = page.locator("div[data-autotest-id=please-enter-your-password-1]")
        this.termsAndPrivacyError = page.locator("id=termsError")
    }

    async fillSignUpDetails(signUpDetails: SignUpDetails, checkTermsAndPrivacy = true, checkNewsAndUpdates = true): Promise<void> {
        await this.nameField.click()
        await this.nameField.type(signUpDetails.name)
        await this.emailField.click()
        await this.emailField.type(signUpDetails.email)
        await this.passwordField.click()
        await this.passwordField.type(signUpDetails.password)

        if (checkTermsAndPrivacy)
            await this.termsAndPrivacyPolicyCheckbox.click()

        if (checkNewsAndUpdates)
            await this.newsAndUpdatesCheckbox.click();
    }

    async getNameFieldPlaceholderText(): Promise<string> {
        return await super.getPlaceholderText(this.nameField)
    }

    async getEmailFieldPlaceholderText(): Promise<string> {
        return await super.getPlaceholderText(this.emailField)
    }

    async getPasswordFieldPlaceholderText(): Promise<string> {
        return await super.getPlaceholderText(this.passwordField)
    }

    async areElementsVisible(): Promise<boolean> {
        return await this.getStartedLabel.isVisible() &&
            this.noCardRequiredLabel.isVisible() &&
            this.nameField.isVisible() &&
            this.emailField.isVisible() &&
            this.passwordField.isVisible() &&
            this.termsAndPrivacyError.isVisible() &&
            this.newsAndUpdatesCheckbox.isVisible() &&
            this.getStartedNowButton.isVisible() &&
            this.termsLink.isVisible() &&
            this.privacyPolicyLink.isVisible()
    }
}

