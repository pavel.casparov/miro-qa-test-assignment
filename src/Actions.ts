import { Page } from "@playwright/test"
import { SignUpDetails } from "./dto/SignUpDetails"
import { MiroHomePage } from "./pageobjects/MiroHomePage"
import { SignUpPage } from "./pageobjects/SignUpPage"

export class Actions {
  static async InitiateSignupFromHomePage(page: Page, locale: string) : Promise<void> {
    const miroHomePage = new MiroHomePage(page)
    if (locale != "en")
      await miroHomePage.changeSiteLocale(locale)
    await miroHomePage.clickOnSignupForFree()
  }

  static async FillAndSubmitSignupForm(signUpPage: SignUpPage, details: SignUpDetails, checkTermsAndPrivacy = true, checkNewsAndUpdates = true) : Promise<void> {
      await signUpPage.fillSignUpDetails(details, checkTermsAndPrivacy, checkNewsAndUpdates)
      await signUpPage.getStartedNowButton.click()
  }
}